﻿using HanamiAPI.Dtos;
using HanamiAPI.Models;
using HanamiAPI.Services;
using Microsoft.AspNetCore.Authorization; // Importe o namespace para usar AllowAnonymous
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HanamiAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Authorize] 
    public class PostsController : ControllerBase
    {
        private readonly IPostsService _postService;

        public PostsController(IPostsService postService)
        {
            _postService = postService;
        }

        [HttpPost]
        public async Task<IActionResult> CreatePost(CreatePostsDto createPostDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await _postService.AddPostAsync(createPostDto);
            return Ok();
        }

        [HttpGet]
        [AllowAnonymous] 
        public async Task<ActionResult<IEnumerable<Posts>>> GetPosts()
        {
            var posts = await _postService.GetPostsAsync();
            return Ok(posts);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Posts>> GetPostById(int id)
        {
            var posts = await _postService.GetPostByIdAsync(id);
            if (posts == null)
            {
                return NotFound();
            }

            return Ok(posts);
        }
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdatePost(int id, UpdatePostsDto updatePostDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var posts = await _postService.GetPostByIdAsync(id);
            if (posts == null)
            {
                return NotFound();
            }

            await _postService.UpdatePostAsync(id, updatePostDto);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePost(int id)
        {
            var posts = await _postService.GetPostByIdAsync(id);
            if (posts == null)
            {
                return NotFound();
            }

            await _postService.DeletePostAsync(id);
            return NoContent();
        }
    }
}
